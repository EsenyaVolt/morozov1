﻿
// ImageFilteringDlg.h: файл заголовка
//

#pragma once

#include <vector>

using namespace std;

struct complex_number
{
	double real = 0.;
	double image = 0.;
};

struct DotsDome
{
	double x = 0.;
	double y = 0.;
	complex_number value;
	int color = 0;
};

// Диалоговое окно CImageFilteringDlg
class CImageFilteringDlg : public CDialogEx
{
	// Создание
public:
	CImageFilteringDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IMAGEFILTERING_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV

// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//области рисования
	CWnd* PicWndImage;
	CDC* PicDcImage;
	CRect PicImage;

	CWnd* PicWndNoise;
	CDC* PicDcNoise;
	CRect PicNoise;

	CWnd* PicWndSpectrum;
	CDC* PicDcSpectrum;
	CRect PicSpectrum;

	CWnd* PicWndFilter;
	CDC* PicDcFilter;
	CRect PicFilter;

	double xpImage = 0, ypImage = 0,			//коэфициенты пересчета
		xminImage = -1, xmaxImage = 1,			//максисимальное и минимальное значение х 
		yminImage = -0.5, ymaxImage = 5;			//максисимальное и минимальное значение y

	double xpNoise = 0, ypNoise = 0,			//коэфициенты пересчета
		xminNoise = -1, xmaxNoise = 1,			//максисимальное и минимальное значение х 
		yminNoise = -0.5, ymaxNoise = 5;			//максисимальное и минимальное значение y

	double xpSpectrum = 0, ypSpectrum = 0,			//коэфициенты пересчета
		xminSpectrum = -1, xmaxSpectrum = 1,			//максисимальное и минимальное значение х 
		yminSpectrum = -0.5, ymaxSpectrum = 5;			//максисимальное и минимальное значение y

	double xpFilter = 0, ypFilter = 0,			//коэфициенты пересчета
		xminFilter = -1, xmaxFilter = 1,			//максисимальное и минимальное значение х 
		yminFilter = -0.5, ymaxFilter = 5;			//максисимальное и минимальное значение y

public:
	afx_msg void OnBnClickedCalculate();
	afx_msg void OnBnClickedDischarge();
	afx_msg void OnBnClickedDrawimage();
	afx_msg void OnBnClickedDrawnoise();
	afx_msg void OnBnClickedDrawfilter();
	afx_msg void OnBnClickedDrawspectrum();

	CButton radio_model;
	CButton radio_image;
	CButton radio_zero;
	CButton radio_interpolation;
	CButton radio_line;
	CButton radio_logarithm;
	CButton radio_rectangle;
	CButton radio_circle;

	int value_width;
	int value_height;
	int value_noise;
	int value_filter;
	double value_SKO;

	vector<vector<DotsDome>> GaussModel;
	vector<vector<DotsDome>> GaussModelNoise;
	vector<vector<DotsDome>> GaussModelSpectrum;
	vector<double> CoordWindow;
	vector<vector<DotsDome>> GaussModelFilter;

	double GaussComponent(double x, double y, double A, double mathX, double mathY, double disp);
	vector<vector<DotsDome>> GaussDome(vector<double> parameter);
	vector<vector<DotsDome>> GaussCalculate();
	CImage ImageUpload();
	vector<vector<DotsDome>> WriteImageToVector(CImage img);
	void Mashtab(vector<vector<DotsDome>>& solve_buff, double* mmin, double* mmax);
	void Mashtab(vector<double>& solve_buff, double* mmin, double* mmax);
	vector<double> GetRangeColors(double min, double max);
	int GetColorPixel(double value, vector<double> helpPix);
	vector<vector<DotsDome>> FillingFieldColor(vector<vector<DotsDome>> vec, vector<double> value);
	vector<vector<DotsDome>> FillingFieldColor(vector<vector<DotsDome>> vec, vector<double> module, vector<double> value);
	double RandStaff();
	vector<double> GenerationNoise(vector<vector<DotsDome>> Gauss);
	vector<vector<DotsDome>> ImposeNoise(vector<vector<DotsDome>> vec, vector<double> vec_noise);
	int NumberDegreeTwo(int width_height);
	vector<complex_number> fourea(vector<complex_number> data, int n, int is);
	vector<vector<DotsDome>> SupplementZero(vector<vector<DotsDome>> vec);
	vector<DotsDome> InterpolationVector(vector<DotsDome> vec, int new_size, bool is_width);
	vector<vector<DotsDome>> SupplementInterpolation(vector<vector<DotsDome>> vec);
	vector<vector<DotsDome>> GetSpectrum(vector<vector<DotsDome>> vec);
	vector<double> GetModuleSpectrum(vector<vector<DotsDome>> vec);
	vector<vector<vector<DotsDome>>> HighlightQuadrants(vector<vector<DotsDome>> vec);
	vector<vector<DotsDome>> SwapQuarters(vector<vector<DotsDome>> vec);
	vector<vector<vector<DotsDome>>> TurnQuarter(vector<vector<vector<DotsDome>>> vec); 
	vector<double> SelectFilteringArea(vector<vector<DotsDome>> vec);
	vector<vector<DotsDome>> NullifyTheExcess(vector<vector<DotsDome>> vec, int radius);
	vector<vector<DotsDome>> RecoverySpectrum(vector<vector<DotsDome>> vec);
	vector<vector<DotsDome>> RemoveUnnecessary(vector<vector<DotsDome>> vec);
	

	void DrawGauss(vector<vector<DotsDome>> vec, CDC* WinDc, CRect WinxmaxGraphc);
	void DrawGaussNoise(vector<vector<DotsDome>> vec, CDC* WinDc, CRect WinxmaxGraphc);
	void DrawGaussSpectrum(vector<vector<DotsDome>> vec, vector<double> Coord, CDC* WinDc, CRect WinxmaxGraphc);
	void DrawGaussFilter(vector<vector<DotsDome>> vec, CDC* WinDc, CRect WinxmaxGraphc);
};